const argon2 = require('argon2');
const crypto = require('crypto');

module.exports = {

	/**
	 * Compute a hash of a password, to store it securely
	 *
	 * @param password Password to hash
	 * @param options  Argon2 algorithm sec options
	 * @return the hash in format 
	 * "$argon2d$v=19$m=128000,t=40,p=4$sfSe5MewORVlg8cDtxOTbg$uqWx4mZvLI092oJ8ZwAjAWU0rrBSDQkOezxAuvrE5dM"
	 */
    argon2Hash: async function(password, options) {
		options = options || {};
		options = {
			...options,
			timeCost: process.env.SEC_ITERATIONS,
			memoryCost: process.env.SEC_MEMORY, 
			parallelism: process.env.SEC_PARALLELISM, 
			type: argon2.argon2d,
			raw: false
		};

		try {
			return await argon2.hash(password, options);
		} catch (err) {
			console.log(err.toString());
			throw err;
		}		
	},

	/**
	 * Verifies a password against a hash
	 *
	 * @param hash     Hash to verify
	 * @param password Password to which hash must be verified against
	 * @return True if the password matches the hash, false otherwise.
	 */
    argon2Verify: async function(hash, password) {
		try {
			if (await argon2.verify(hash, password)) {
				return true;
			} else {
				return false;
			}
		} catch (err) {
			console.log(err.toString());
			throw err;
		}	
	},

	/**
	 * Compute a SHA256 hash of the input, using the secret configured in the environment.
	 * 
	 * @param input	The input string to hash
	 * @returns an hex string of the hashed input
	 */
	sha256Hash: function(input) {
		var hmac = crypto.createHmac('sha256', process.env.SEC_HMAC_SECRET);
		return hmac.update(input).digest('hex');
	}

}