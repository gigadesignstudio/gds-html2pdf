module.exports = {

    /**
     * Loads local .env file variables when in dev
     */
    loadLocalEnvVariables: function() {
        if (process.argv[2] && process.argv[2] == 'dev') {
            var dotenv = require('dotenv');
            dotenv.load();
        }
	}
	
}