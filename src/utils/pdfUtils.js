const puppeteer = require('puppeteer');
const urlModule = require("url");
const URL = urlModule.URL;

module.exports = {

    /**
     * Use Google Puppeteer to simulate navigation to URL and page printing
	 * @param url url to navigate
	 * @param options options to give to Page.pdf() puppeteer call
     */
    getPdfFromUrl: async function(url, options) {

		options = options || {
			format: 'A4',
			printBackground : true
		};

        const browser = await puppeteer.launch();
		const page = await browser.newPage();
		const stylesheetContents = {};

		// 1. Stash the responses of local stylesheets.
		page.on('response', async resp => {
			const responseUrl = resp.url();
			const sameOrigin = new URL(responseUrl).origin === new URL(url).origin;
			const isStylesheet = resp.request().resourceType() === 'stylesheet';
			if (sameOrigin && isStylesheet) {
				stylesheetContents[responseUrl] = await resp.text();
			}
		});

		// 2. Load page as normal, waiting for network requests to be idle.
		await page.goto(url, {waitUntil: 'networkidle0'});

		// 3. Inline the CSS.
		// Replace stylesheets in the page with their equivalent <style>.
		await page.$$eval('link[rel="stylesheet"]', (links, content) => {
			links.forEach(link => {
				const cssText = content[link.href];
				if (cssText) {
					const style = document.createElement('style');
					style.textContent = cssText;
					link.replaceWith(style);
				}
			});
		}, stylesheetContents);

		// 4. return a PDF and close the browser.
		var pdf = await page.pdf(options);
		await browser.close();

		return pdf;
	}
	
}