module.exports = {

    onMongoError: function(err, rethrow) {
		console.log(err.message);
		if (rethrow)
			throw new Error(err);
	},
	
}