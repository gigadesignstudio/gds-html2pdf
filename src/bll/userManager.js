const User = require('../dal/user');
const errorUtils = require('../utils/errorUtils');

exports = module.exports = {};

exports.getByEmail = async function(email) {

	var query = User.findOne({
		email: email
	});
	return await query.exec();

}

exports.create = async function(user) {

	user.save(function (err, user) {
		if (err) {
			errorUtils.onMongoError(err);
			return false;
		}
		return user;
	});

}