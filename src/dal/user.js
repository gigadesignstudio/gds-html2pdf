var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

var modelSchema = new Schema({

    // sign up fields
    name:  {
        type: String,
        required: true
    },
    surname:  {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    secAnswer: {
        type: String,
        required: true
    },

    // non mandatory fields, post sign up
    profilePictureUrl: String,
    privacy: {
        type: String,
        enum: ['all', 'none', 'third-party'],
        required: true,
        default: 'none'
    },
    userStatus: {
        type: String, 
        enum: ['active', 'inactive', 'banned'],
        required: true,
        default: 'active'
    },
    userType: {
        type: String, 
        enum: ['privato', 'pubblico'],
        required: true,
        default: 'pubblico'
    },
    paymentStatus: {
        type: String, 
        enum: ['ok', 'ko'],
        required: true,
        default: 'ok'
    },
    role: {
        type: String,
        enum: ['simple', 'admin', 'superuser'],
        required: true,
        default: 'simple'
    },

    // automatici mongoose
    updatedAt: {
        type: Date,
        default: Date.now
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', modelSchema);