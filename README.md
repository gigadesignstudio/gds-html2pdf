# README #

gds's fatturator

### TO DO (s) ###

### package info ###
* The tilde matches the most recent minor version (the middle number). ~1.2.3 will match all 1.2.x versions but will miss 1.3.0.
* The caret matches the most recent major version (the first number). ^1.2.3 will match any 1.x.x release including 1.3.0, up to 2.0.0.

## SEC STEPS ##
* brew install gcc
* npm install -g node-gyp
* CXX=g++ npm install argon2

## SEC BEST PRACTICES ##
* limit password to 128 chars to avoid DOS when user inserts super long password
* follow https://hackernoon.com/your-node-js-authentication-tutorial-is-wrong-f1a3bf831a46 to develop "recover password" steps

## CODE ##
* spostare invoice views in cartella apposita