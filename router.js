const fetch = require('node-fetch');

module.exports = {
	
	/**
	 * register all routes
	 * @param app express
	 */
	registerRoutes: (app) => {
		const indexRouter = require('./routes/index');
		const invoiceRouter = require('./routes/invoice/router');
		const authRouter = require('./routes/auth/router');

		app.use('/', indexRouter);
		app.use('/invoice', invoiceRouter);
		app.use('/auth', authRouter);
	}

};
  