const mongoose = require('mongoose');

exports = module.exports = {};

exports.init = function() {

	var isConnectedBefore = false;

	var connectWithRetry = function () {
		return mongoose.connect(process.env.CONNECTION_STRING,
		{ useNewUrlParser: true },
		function (err) {
			if (err) {
				console.error('Failed to connect to DB - retrying in 5 sec', err);
				setTimeout(connectWithRetry, 5000);
			}
		});
	};
	connectWithRetry();

	mongoose.connection.on('error', function () {
		console.log('Could not connect to DB');
	});

	mongoose.connection.on('disconnected', function () {
		console.log('Lost DB connection...');
		if (!isConnectedBefore)
			setTimeout(connectWithRetry, 5000);
	});
	mongoose.connection.on('connected', function () {
		isConnectedBefore = true;
		console.log('Connection established to DB');
	});

	mongoose.connection.on('reconnected', function () {
		console.log('Reconnected to DB');
	});

	// Close the Mongoose connection, when receiving SIGINT
	process.on('SIGINT', function () {
		mongoose.connection.close(function () {
			console.log('Force to close the DB conection');
			process.exit(0);
		});
	});

}