const express = require('express');
const router = express.Router();
const fs = require('fs');

/**
 * register all invoice routes within the folder.
 * each one is registered with the same name as the filename
 */
fs.readdirSync(__dirname).forEach( function(route) {

	route = route.split('.')[0];
	if (route === 'router') return;
	router.use('/' + route, require('./' + route));
	
});

module.exports = router;