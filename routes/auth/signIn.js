const express = require('express');
const router = express.Router();

const secUtils = require('../../src/utils/secUtils');
const userManager = require('../../src/bll/userManager');
const User = require('../../src/dal/user');

router.post('/', 
	async function(req, res) {
		
		var user = new User({
            email : req.body.email,
            password : req.body.password
		});

		var storedUser = await userManager.getByEmail(user.email);
		if (storedUser != null) {
			var result = await secUtils.argon2Verify(storedUser.password, secUtils.sha256Hash(user.password));
			if (result)
				return res.redirect('/');
		}
		
		return res.redirect('/auth/login');
	}
);

module.exports = router;