const express = require('express');
const router = express.Router();

const secUtils = require('../../src/utils/secUtils');
const userManager = require('../../src/bll/userManager');
const User = require('../../src/dal/user');
  
router.post('/', 
    async function(req, res) {

        var user = new User({
            name : req.body.name,
            surname : req.body.surname,
            email : req.body.email,
            secAnswer : req.body.securityAnswer,
            password : req.body.password
        });

        var storedUser = await userManager.getByEmail(user.email);
        if (storedUser == null) {
            user.password = await secUtils.argon2Hash(secUtils.sha256Hash(user.password));
            userManager.create(user);
        }
    
        return res.redirect('/auth/login');
    }
);

// router.get('/profile',
//     require('connect-ensure-login').ensureLoggedIn(),
//     function(req, res){
//         res.render('profile', { user: req.user });
//     }
// );

module.exports = router;