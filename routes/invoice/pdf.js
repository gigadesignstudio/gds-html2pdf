const express = require('express');
const router = express.Router();
const pdfUtils = require('../../src/utils/pdfUtils');

/**
 * get an invoice PDF, by its id
 */
router.get('/:id', async function(req, res, next) {

    var fullUrl = req.protocol + '://' + req.get('host') + '/invoice/html/' + req.params.id;
    var pdf = await pdfUtils.getPdfFromUrl(fullUrl);

    res.setHeader("Content-Type", "application/pdf");
    res.send(pdf);
  
});

module.exports = router;
